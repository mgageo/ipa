# <!-- coding: utf-8 -->
#
# quelques fonctions mnhn
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
#
mnhnDir <- sprintf("%s/bvi35/CouchesMNHN", Drive)
mnhn <- function() {
  mnhn_taxref_aves(version='v9.0')
  mnhn_taxref_aves(version='v10.0')
}
mnhn_lof_lire <- function() {
  f_csv <-  sprintf('%s/web.var/geo/MNHN/LOF_MAJ_2010.txt', Drive)
  if (! file.exists(f_csv) ) {
    stop(sprintf("mnhn_lof_lire() f_csv:%s", f_csv))
  }
  con  <- file(f_csv, open = "r")
  i <- 0
  lof <- matrix(data=NA, nrow=0, ncol=4)
  colnames(lof) <- c("groupe", "categorie", "scientifique", "espece")
  while (length(ligne <- readLines(con, n = 1, warn = FALSE)) > 0) {
    i <- i + 1
    if ( i == 1 ) {
      next
    }
#    Log(sprintf("i:%d ligne:%s", i, ligne))
    champs <- unlist(strsplit(ligne, "\\t"))
    if ( champs[3] == "" ) {
      next
    }
    if ( ! grepl("[[:lower:]]", champs[3], perl=TRUE) ) {
      groupe <- champs[3]
      next
    }
#    Log(sprintf("i:%d NOM:%s", i, champs[3]))
    lof <- rbind(lof, c(groupe, champs[2], champs[3], champs[4]))
  }
  close(con)
#  print(head(lof))
  return(as.data.frame(lof))
}
# source("geo/scripts/lrr.R");mnhn_taxref_aves()
mnhn_taxref_aves <- function(f_taxref = NULL, f_aves = NULL, version='v13.0') {
  library(writexl)
  f_taxref <- sprintf("%s/telechargement/TAXREF_INPN_v9_0/TAXREFv90.txt", mnhnDir)
  if ( grepl('v10', version) ) {
    f_taxref <- sprintf("%s/telechargement/TAXREF_INPN_v10_0/TAXREFv10.0.txt", mnhnDir)
  }
  if ( grepl('v11', version) ) {
    f_taxref <- sprintf("%s/telechargement/TAXREF_INPN_v11/TAXREFv11.txt", mnhnDir)
  }
  if ( grepl('v12', version) ) {
    f_taxref <- sprintf("%s/telechargement/TAXREF_INPN_v12/TAXREFv12.txt", mnhnDir)
  }
  if ( grepl('v13', version) ) {
    f_taxref <- sprintf("%s/telechargement/TAXREF_INPN_v13/TAXREFv13.txt", mnhnDir)
  }
  carp("version : %s dsn: %s", version, f_taxref)
  mnhn.df <- mnhn_taxref_lire(f_taxref)
#  print(head(mnhn.df))
  aves.df <- subset(mnhn.df, CLASSE=="Aves")
#  print(head(aves.df))
#  stop("***")
  aves.df <- aves.df[, c("CD_NOM", "CD_REF", "ORDRE", "FAMILLE", "NOM_COMPLET", "NOM_VALIDE", "NOM_VERN")]
  dsn <- f_aves
  if ( is.null(dsn) ) {
    dsn <- sprintf("%s/AVES%s.csv", mnhnDir, version)
  }
  df_ecrire(aves.df, dsn);
  carp("dsn:%s nb:%d", dsn, nrow(aves.df))
  dsn <- sprintf("%s/AVES%s.xlsx", mnhnDir, version)
  writexl::write_xlsx(aves.df, dsn)
  carp("dsn:%s nb:%d", dsn, nrow(aves.df))
}
mnhn_taxref_lire <- function(f_csv = NULL) {
  library(rio)
#  df <- read.table(file = f_csv, head=TRUE, sep = '\t', quote = '', encoding="UTF-8")
  df <- import(f_csv, encoding = "UTF-8")
  carp("f_csv:%s nb:%d", f_csv, nrow(df))
  return(df)
}
mnhn_taxref_aves_lire <- function(version = "v13.0") {
  library(rio)
  dsn <- sprintf("%s/AVES%s.xlsx", mnhnDir, version)
  df <- import(dsn)
  carp("dsn:%s nb:%d", dsn, nrow(df))
  return(invisible(df))
}
#
# la partie taxref
mnhn_taxref_ecrire_v1 <- function() {
  mnhn.df <- mnhn_taxref_lire(f_taxref)
  aves.df <- subset(mnhn.df, CLASSE=="Aves")
  aves.df <- aves.df[, c("CD_NOM", "ORDRE", "FAMILLE", "NOM_COMPLET", "NOM_VALIDE", "NOM_VERN")]
  dsn <- sprintf("%s/AVES%s.csv", varDir, version)
  df_ecrire(aves.df, dsn);
}
#
# la partie taxref
mnhn_taxref_ecrire <- function(version='v10.0') {
  Log(sprintf("mnhn_taxref_ecrire() version : %s", version))
  f_taxref <- sprintf("%s/telechargement/TAXREF_INPN_v9_0/TAXREFv90.txt", mnhnDir)
  if ( grepl('v10', version) ) {
    f_taxref <- sprintf("%s/telechargement/TAXREF_INPN_v10_0/TAXREFv10.0.txt", mnhnDir)
  }
  mnhn.df <- mnhn_taxref_lire(f_taxref)
  aves.df <- subset(mnhn.df, CLASSE=="Aves")
  aves.df <- subset(aves.df, CD_NOM==CD_REF)
  aves.df <- aves.df[, c("CD_NOM", "ORDRE", "FAMILLE", "NOM_COMPLET", "NOM_VALIDE", "NOM_VERN")]
  dsn <- sprintf("%s/AVES%s.csv", especesDir, version)
  df_ecrire(aves.df, dsn);
}
mnhn_aves_lire <- function(version='v10.0') {
  dsn <- sprintf("%s/AVES%s.csv", varDir, version)
  df <- df_lire(dsn);
  Log(sprintf("mnhn_taxref_lire) dsn:%s nb:%d", dsn, nrow(df)))
  return(df)
}
#
# pb avec TAXO_LATIN "Motacilla flavissima" et NOM_VALIDE
# passage avec NOM_COMPLET
# https://inpn.mnhn.fr/espece/cd_nom/3741/tab/taxo
mnhn_especes_taxref <- function(especes.df, x="TAXO_LATIN", y="NOM_COMPLET" ) {
  library(stringi)
  Log(sprintf("mnhn_especes_taxref() especes.df nb: %d", nrow(especes.df)))
  dsn <- sprintf("%s/AVESv9.0.csv", varDir)
  taxref.df <- df_lire(dsn)
  taxref.df[] <- lapply(taxref.df, function(x) stri_encode(x, "", "UTF-8"))
#  Log(sprintf("mnhn_especes_taxref() taxref.df encoding:%s", Encoding(taxref.df$NOM_VERN) ))
  taxref.df <- subset(taxref.df, CD_NOM==CD_REF)
  df <- taxref.df[grep(",", taxref.df$NOM_VERN),]
  print(sapply(df, class))
  df1 <- df
  df1$NOM_VERN <- sub(",.*", "", df1$NOM_VERN)
  df2 <- df
  df2$NOM_VERN <- sub(".*, ", "", df2$NOM_VERN)
  print(head(df2[grepl("Chev", df2$NOM_VERN),] ))
  taxref.df <- rbind(taxref.df, df1, df2)
  taxref.df$espece_c <- camel3(taxref.df$NOM_VERN)
  print(head(taxref.df[grepl("Chev.*Ath",taxref.df$NOM_VERN),] ))
  stop("especes()")
  especes.df$espece_c <- camel3(especes.df$lib_species)
#  print(head(df))
#  print(head(taxref.df))
  Log(sprintf("mnhn_especes_taxref() especes.df"))
  print(sapply(especes.df, class))
  Log(sprintf("mnhn_especes_taxref() taxref.df"))
  print(sapply(taxref.df, class))
  df <- merge(x=especes.df, y=taxref.df, by.x="espece_c", by.y="espece_c", all.x=TRUE, all.y=FALSE, sort=FALSE)
  inconnuDF <- subset(df, is.na(df$CD_NOM))
  if ( length(inconnuDF$CD_NOM) > 0 ) {
    Log(sprintf("especes() espece invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF, 30))
    stop("especes()")
  }
  print(head(df))
  Log(sprintf("mnhn_especes_taxref() df nb: %d", nrow(df)))
  return(df)
}
#
# les données protection du mnhn/inpn
mnhn_protection_ecrire <- function() {
  library("xlsx")
#  library(tidyr)
  library(reshape2)
  f <- sprintf("%s/telechargement/ESPECES_REGLEMENTEES/PROTECTION_ESPECES_10.xlsx", varDir)
  Log(sprintf("mnhn_protection_ecrire() f:%s",f))
  df <- read.xlsx(f, sheetName = "PROTECTION_ESPECES_EXPORT", header=TRUE, colClasses=NA, stringsAsFactors=FALSE)
  Log(sprintf("mnhn_protection_ecrire() nrow: %d", nrow(df)))
  print(head(df))
#  df <- dcast(df, CD_REF + NOM_FRANCAIS + NOM_SCIENTIFIQUE ~ TYPE_STATUT, value.var = "CATEGORIE")
  print(head(df))
  dsn <- sprintf("%s/especes_protection.csv", varDir)
  df_ecrire(df, dsn);
}
#
# les données status du mnhn/inpn taxref v13
# source("geo/scripts/zac.R");mnhn_status_lire()
mnhn_status_lire <- function() {
  library(tidyverse)
  library(readr)
  library(knitr)
  library(kableExtra)
  carp()
  if ( ! exists("mnhn_statuts.df") ) {
    dsn <- sprintf("%s/BDC-Statuts-v13/BDC_STATUTS_13.csv", mnhnDir)
    mnhn_statuts.df <<- read_delim(dsn, delim = "," )
  }
  return(invisible(mnhn_statuts.df))
  df1 <- mnhn_statuts.df  %>%
    filter(grepl("Streptopelia turtur", LB_NOM)) %>%
    filter(LB_ADM_TR == "Bretagne") %>%
#    group_by(CD_NOM, CD_TYPE_STATUT, LB_TYPE_STATUT, LB_ADM_TR, NIVEAU_ADMIN, LABEL_STATUT) %>%
#    summarize(nb= n()) %>%
    glimpse()
  print(knitr::kable(df1, format = "pipe"))
}
#
# wetlands : https://inpn.mnhn.fr/espece/inventaire/T138_1
mnhn_wetlands_lire <- function() {
  dsn <- sprintf("%s/telechargement/wetlands_2017128.csv", mnhnDir)
  df <-  df <- read.table(dsn, header=TRUE, sep=";", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote='"', encoding="UTF-8")
#  print(head(df))
  return(df)
}
#
# on complète avec le taxref
mnhn_wetlands_taxref <- function() {
  wetlands.df <- mnhn_wetlands_lire()
  taxref.df <- mnhn_aves_lire("v9.0")
  df <- merge(wetlands.df, taxref.df, by.x="CD_NOM", by.y="CD_NOM", all.x=TRUE, all.y=FALSE, sort=FALSE)
  inconnuDF <- subset(df, is.na(df$CD_NOM))
  if ( length(inconnuDF$CD_NOM) > 0 ) {
    Log(sprintf("mnhn_wetlands_liste() espece invalide nb: %d", nrow(inconnuDF)))
    print(head(inconnuDF, 10))
    stop("mnhn_wetlands_taxref()")
  }
  print(head(df))
  return(df)
}
mnhn_taxref_ordre_famille <- function() {
  df <- mnhn_aves_lire("v9.0")
#  print(head(df))
  champs <- c("ORDRE", "FAMILLE")
  df <- data.frame(table(df[, champs]), stringsAsFactors=FALSE)
  colnames(df) <- append(champs, "nb")
  df <- subset(df, nb > 0 )
  return(df)
}

mnhn_taxref_clean <- function() {
  df <- read.table(text="regexp
Petit contrefaisant
Hirondelle de chemin.*
Pivert
Pinson des Ardennes
Puffin de Scopoli
Loriot jaune
Gravelot de Kent
M.*sange .* moustaches
Traquet tarier
Chouette effraie
Petit pingouin
P.*trel cul.blanc
P.*trel temp.*te
P.*trel fulmar
Chouette chev.*che
Nyctale de Tengmalm
Ouette
Niverolle alpine
Outarde
Grand Corbeau
Faisan
Pinson du Nord
Puffin des Anglais
Martinet.*ventre blanc
Garrot.*or
Harelde boréale
Tadorne casarca
Blongios
penduline
Crabier chevelu
Bihoreau
Tal.*sultane
Harelde
Monticole
Fuligule.*bec
Cassenoix
Bihoreau
Hypo.*ict
Petit.*scops
thrix jaune
Combattant vari
Barge rousse
Caille des bl
Cormoran Huppé
Courlis Corlieu
Cygne De Bewick
Faucon.*lerin
Marouette.*baillon
Sterne.*Dougall
B.* m
Cormoran H
", header=TRUE, sep="|", blank.lines.skip = TRUE, stringsAsFactors=FALSE, quote="")
  dsn <- sprintf("%s/AVESv10.0.csv", especesDir)
  taxref.df <- df_lire(dsn);
  taxref.df <- subset(taxref.df, CD_NOM == CD_REF)
  nom.df <- data.frame(CD_NOM=character(),NOM_VERN=character(), stringsAsFactors=FALSE)
  for(i in 1:nrow(df) ) {
    regexp <- df$regexp[i]
    df1 <- subset(taxref.df, grepl(regexp, NOM_VERN, ignore.case=TRUE))
    nom.df <- rbind(nom.df, df1[, c("CD_NOM", "NOM_VERN")])
  }
  dsn <- sprintf("%s/taxref_clean.csv", especesDir)
  df_ecrire(nom.df, dsn);
}
mnhn_especes_clean <- function(v) {
  v <- gsub(", Petit contrefaisant", "", v)
  v <- gsub(", Hirondelle de chemin.*", "", v)
  v <- gsub(", Pivert", "", v)
  v <- gsub(", Pinson des Ardennes", "", v)
  v <- gsub(", Puffin de Scopoli", "", v)
  v <- gsub(", Loriot jaune", "", v)
  v <- gsub(", Gravelot de Kent", "", v)
  v <- gsub(", M.*sange .* moustaches", "", v)
  v <- gsub(", Traquet p.*tre", "", v)
  v <- gsub("Traquet tarier, ", "", v)
  v <- gsub("Chouette effraie, ", "", v)
  v <- gsub("Petit pingouin, ", "", v)
  v <- gsub(".*trel cul.blanc, ", "", v)
  v <- gsub(".*trel temp.*te, ", "", v)
  v <- gsub(".*trel fulmar, ", "", v)
  v <- gsub("Chouette chev.*che, ", "", v)
  v <- gsub("Nyctale de Tengmalm, ", "", v)
  return(v)
}
mnhn_taxref_v1 <- function() {
  dsn <- sprintf("%s/AVESv10.0.csv", especesDir)
  taxref.df <- df_lire(dsn);
  taxref.df <- subset(taxref.df, CD_NOM == CD_REF)
#  df <- subset(taxref.df, grepl(",", taxref.df$NOM_VERN))
#  df$NOM_VERN <- gsub(",.*", "", df$NOM_VERN)
#  taxref.df <- rbind(taxref.df, df)
#  df <- subset(taxref.df, grepl(",", taxref.df$NOM_VERN))
#  df$NOM_VERN <- gsub(".*, ", "", df$NOM_VERN)
#  taxref.df <- rbind(taxref.df, df)
  taxref.df <- subset(taxref.df, CD_NOM != 2442) ; # Grand Cormoran
  taxref.df <- subset(taxref.df, CD_NOM != 3300) ; # Goéland brun
  taxref.df <- subset(taxref.df, CD_NOM != 4497) ; # Choucas des tours
  taxref.df <- subset(taxref.df, CD_NOM != 3943) ; # Bergeronnette grise
  taxref.df <- subset(taxref.df, CD_NOM != 728082) ; # Moineau domestique
  taxref.df <- subset(taxref.df, CD_NOM != 653924) ; # Grande Aigrette
  taxref.df <- subset(taxref.df, CD_NOM != 728061) ; # Grande Aigrette
  taxref.df <- subset(taxref.df, CD_NOM != 1958) ; # Sarcelle d'hiver
  taxref.df$NOM_VERN <- gsub(", Petit contrefaisant", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(", Hirondelle de chemin.*", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(", Pivert", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(", Pinson des Ardennes", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(", Puffin de Scopoli", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(", Loriot jaune", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(", Gravelot de Kent", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(", M.*sange .* moustaches", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub("Traquet tarier, ", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub("Chouette effraie, ", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub("Petit pingouin, ", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(".*trel cul.blanc, ", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(".*trel temp.*te, ", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub(".*trel fulmar, ", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub("Chouette chev.*che, ", "", taxref.df$NOM_VERN)
  taxref.df$NOM_VERN <- gsub("Nyctale de Tengmalm, ", "", taxref.df$NOM_VERN)
  return(taxref.df)
}
mnhn_taxref_mga <- function() {
  dsn <- sprintf("%s/taxref_mga.csv", especesDir)
  df <- df_lire(dsn);
  dsn <- sprintf("%s/AVESv10.0.csv", especesDir)
  taxref.df <- df_lire(dsn);
  taxref.df <- subset(taxref.df, CD_NOM == CD_REF)
  nom.df <- data.frame(CD_NOM=character(),NOM_VERN=character(), stringsAsFactors=FALSE)
  for(i in 1:nrow(df) ) {
#    print(sprintf("i:%i CD_NOM : %s", i, df$CD_NOM[i]))
    taxref.df$NOM_VERN[taxref.df$CD_NOM == df$CD_NOM[i]] <- df$NOM_VERN[i]
  }
  return(taxref.df)
}

#
## pour convertir les noms vernaculaires en nom scientifique, code crpbo ------
#
# source("geo/scripts/stoc.R"); df <- mnhn_crpbo_ecrire() %>% glimpse()
mnhn_crpbo_ecrire <- function() {
  carp()
  library(tidyverse)
  library(rio)
  library(janitor)
  dsn <- sprintf('%s/cinfo_sm.xlsx', mnhnDir)
  carp('dsn: %s', dsn)
  cinfo.df <- rio::import(dsn, which='cinfo') %>%
    clean_names() %>%
    glimpse()
  dsn <- sprintf('%s/crpbo_especes.xlsx', mnhnDir)
  carp('dsn: %s', dsn)
  df <- rio::import(dsn, which='codes') %>%
    clean_names() %>%
    glimpse()
  df1 <- df %>%
    left_join(cinfo.df, by=c('nom_scientifique'='nom_scientifique_cinfo'))
  df2 <- df1 %>%
    filter(is.na(ordre)) %>%
    glimpse() %>%
    print(n=50, na.print="")
  df3 <- df1 %>%
    dplyr::select(code_crbpo, nom_scientifique, nom_francais=nom_francais_cinfo) %>%
    glimpse()
  sauve_rds(df3, dsn='crpbo')
  return(invisible(df3))
}
#
# lecture du fichier avec code crpbo
# source("geo/scripts/stoc.R"); df <- mnhn_crpbo_lire() %>% glimpse()
mnhn_crpbo_lire <- function() {
  carp()
  library(tidyverse)
  df <- lire_rds('crpbo')
  return(invisible(df))
}