# <!-- coding: utf-8 -->
#
# quelques fonctions pour les IPA
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
rm(list=ls());
mga  <- function() {
  source("geo/scripts/ipa.R");
}
#
# les variables globales
Drive <- substr( getwd(),1,2)
baseDir <- sprintf("%s/web", Drive)
cbnbrestDir <- sprintf("%s/bvi35/CouchesCBNBrest", Drive);
mnhnDir <- sprintf("%s/bvi35/CouchesMNHN", Drive);
osoDir <- sprintf("%s/bvi35/CouchesOSO", Drive);
varDir <- sprintf("%s/bvi35/CouchesIPA", Drive);
texDir <- sprintf("%s/web/geo/%s", Drive, "IPA")
cfgDir <- sprintf("%s/web/geo/%s", Drive, "IPA")
ipasDir <- sprintf("%s/web/geo/%s", Drive, "IPAS")
dir.create(varDir, showWarnings = FALSE, recursive = TRUE)
dir.create(texDir, showWarnings = FALSE, recursive = TRUE)
ignDir <- sprintf("%s/bvi35/CouchesIGN", Drive);
webDir <- sprintf("%s/web.heb/bv/ipa", Drive);
#
# les bibliothèques
setwd(baseDir)
source("geo/scripts/mga.R");
source("geo/scripts/misc.R")
source("geo/scripts/misc_couples.R")
source("geo/scripts/misc_fonds.R")
source("geo/scripts/misc_geo.R")
source("geo/scripts/misc_gdal.R")
source("geo/scripts/misc_ipa.R")
source("geo/scripts/misc_mnhn.R");
source("geo/scripts/misc_ocs.R");
source("geo/scripts/misc_tex.R");
source("geo/scripts/ipa_chateauneuf.R");
source("geo/scripts/ipa_cojoux.R");
source("geo/scripts/ipa_couches.R");
source("geo/scripts/ipa_donnees.R");
source("geo/scripts/ipa_ipas.R");

#
# les commandes permettant le lancement
DEBUG <- FALSE
if ( interactive() ) {
  carp("interactive")
# un peu de nettoyage
  graphics.off()
} else {
  carp("console")
}
