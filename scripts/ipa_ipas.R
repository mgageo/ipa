# <!-- coding: utf-8 -->
#
# quelques fonctions pour les IPA
# auteur : Marc Gauthier
# licence: Creative Commons Paternité - Pas d'Utilisation Commerciale - Partage des Conditions Initiales à l'Identique 2.0 France
#
# conversion du format ipa en format ipas
# - ajout du site
#
# 15/07/2020
# pointage sur les bons dossiers
# correction perte adresse ip
#
# source("geo/scripts/ipa.R");ipas_jour()
ipas_jour <- function() {
  carp()
  webDir <<- sprintf("%s/web.heb/bv/ipas", Drive);
  ipas_site("../ipa/DONNEES2019", "Cojoux")
  ipas_site("../ipa/DONNEES", "Chateauneuf")
}
ipas_site <- function(dossier, site) {
  carp()
  library(sf)
  library(tidyverse)
  carp()
  dir <- sprintf("%s/%s", webDir, dossier)
  files <- list.files(dir, pattern = "geojson$", full.names = TRUE, ignore.case = TRUE) %>%
    glimpse()
  for (i in 1:length(files)) {
    file <- files[i]
    carp("file: %s", file)
    nc <- st_read(file) %>%
      mutate(site = site)
    fic <- gsub('.*/', '', file)
#    fic <- gsub('\\..*$', '', fic)
    if (grepl("127.0.0.1", fic)) {
      next
    }
    dsn <- sprintf("%s/DONNEES/%s", webDir, fic)
#    st_write(nc, dsn, delete_dsn = TRUE)
    carp("dsn: %s", dsn)
  }
}
#
# source("geo/scripts/ipa.R");ipas_fabrice()
ipas_fabrice <- function() {
  library(readxl)
  library(sf)
  dsn <- sprintf("%s/points_fabrice.xlsx", ipasDir)
  sheets <- excel_sheets(dsn)
  for (i in 1:length(sheets)) {
    sheet <- sheets[i]
    df <- read_excel(dsn, sheet = sheet)
    nc <- st_as_sf(df, coords = c("long.", "lat."), crs = 4326, remove=TRUE) %>%
      dplyr::select(id = point) %>%
      mutate(id = sprintf("%s", id)) %>%
      glimpse()
    dest <- sprintf("%s/%s.geojson", ipasDir, sheet)
    st_write(nc, dest, delete_dsn=TRUE, driver='GeoJSON')
  }
}
#
# les données de 2021
# source("geo/scripts/ipa .R"); df <- ipas_2021()
ipas_2021 <- function() {
  carp()
  library(tidyverse)
  library(jsonlite)
  library(janitor)
  webDir <<- sprintf("%s/web.heb/bv/ipas", Drive);
  path <- sprintf("%s/DONNEES", webDir)
  files <- list.files(path, pattern = "2021.*geojson", full.names = TRUE, ignore.case = TRUE, recursive = TRUE) %>%
    glimpse()
  df <- data.frame()
  for (file in files) {
#    carp("file: %s", file)
    if ( grepl("127.0.0.1", file)) {
      next;
    }
    try({
      json <- jsonlite::fromJSON(file)
    }, silent = TRUE)
    if ( ! exists("json") ) {
      carp("*** file: %s", file)
      next
    }
    if ( ! exists("features", where = json) ) {
      carp("*** features file: %s", file)
      glimpse(json)
      stop("***")
    }
    if ( ! exists("properties", where = json$features) ) {
      carp("*** properties file: %s", file)
      glimpse(json)
      stop("***")
    }
    df1 <- json$features$properties
    df1 <- dplyr::select(df1, matches("[a-z]"))
#    glimpse(df1);stop("***")
#    df1 <- dplyr::select(df1, dateheure, point)
    df <- rbind(df, df1)
  }
  glimpse(df)
  df1 <- df %>%
    filter(! grepl("mga.gauthier", point_email)) %>%
    filter(is.na(label)) %>%
    group_by(point_prenom, site, point, point_dateheure) %>%
    summarize(nb = n())
  print(knitr::kable(df1, format = "pipe"))
}
# source("geo/scripts/ipa .R"); df <- ipas_2021()
ipas_2021 <- function() {
  carp()
  library(tidyverse)
  library(sf)
  library(janitor)
  library(lubridate)
  webDir <<- sprintf("%s/web.heb/bv/ipas", Drive);
  path <- sprintf("%s/DONNEES", webDir)
  files <- list.files(path, pattern = "202[01].*geojson", full.names = TRUE, ignore.case = TRUE, recursive = TRUE) %>%
    glimpse()
  nc <- data.frame()
  for (file in files) {
#    carp("file: %s", file)
    if ( grepl("(127.0.0.1|82.64.4.74)", file)) {
      next;
    }
    try({
      nc1 <- st_read(file, stringsAsFactors=FALSE, quiet = TRUE)
    }, silent = TRUE)
    if ( ! exists("nc1") ) {
      carp("*** file: %s", file)
      next
    }
    cols <- colnames(nc)
    cols1 <- colnames(nc1)
    if ("carre" %in% cols1) {
      colnames(nc1)[colnames(nc1) == "carre"] <- "point"
      cols1 <- colnames(nc1)
    }
    if (length(setdiff(cols, cols1)) != 0) {
      print(sort(cols))
      print(sort(cols1))
      glimpse(nc)
      glimpse(nc1)
    }
    nc <- rbind(nc, nc1)
  }
  glimpse(nc)
  df <- st_drop_geometry(nc)
  df <- cbind(df, st_coordinates(nc)) %>%
    glimpse()
  df1 <- df %>%
    filter(! grepl("mga.gauthier", point_email)) %>%
    filter(is.na(label)) %>%
    mutate(horodatage = dmy_hm(point_dateheure)) %>%
    group_by(point_prenom, site, point, horodatage) %>%
    summarize(nb = n())
  print(knitr::kable(df1, format = "pipe"))
}